# Reddit Bot
A simple Telegram bot that forwards contents posted on a particular subreddit.

## Getting Started
Clone the project or download the source files.
To clone the project use the command:
```
git clone https://Psico94@bitbucket.org/Psico94/reddittelegrambot.git
```

### Prerequisites
To make the project work you need to have python version 3 and pip installed.
To install python [go here](https://realpython.com/installing-python/) and follow the instructions.

### Installing
Once python is installed you need to create a virtual environment and install all the libraries the bot needs to work.

* For the creation of the vitual environment follow the instructions [given here](https://uoa-eresearch.github.io/eresearch-cookbook/recipe/2014/11/26/python-virtual-env/).
* Once you have created the environment you can easily install the libraries using the "Requirements.txt" file supplied with the python file.

To use it run the following commands:
Activate the virtual environment.
```
$ source <env_name>/bin/activate
```
Run the command to install the libraries contained in the requirements.
```
(<env_name>)$ pip install -r path/to/Requirements.txt
```

### Settings
This script only represents the bot logic but does not implement the bot itself.
In order to use the bot you need to follow a series of steps:

* First of all you will need to create a new telegram bot. Find out how to do it [here](https://core.telegram.org/bots).
* Next you will need a reddit account in order to use the APIs and get the bot working. You can register for free on reddit [here](https://www.reddit.com/).
* Once you have created an account, go to the section dedicated to [applications](https://www.reddit.com/prefs/apps). Here you can define a new app so now you have the datas that must be used in order to access the contents published on reddit.
* The next step is to set the reddit app data and the secret telegram bot token inside the bot script. (To know the secret token related to your bot, consult the Telegram [BotFather](https://t.me/botfather)).
* Finally, you can open the script and set login credentials of your reddit account and the subreddit name.

## Usage
### Bot Side
Once you have configured the environment and set the necessary values, you can use the bot.
To use the bot simply activate your virtual environment.
```
$ source <env_name>/bin/activate
```
And start the python script.
```
(<env_name>)$ python RedditBot.py
```
Once started the bot will run as a daemon and it will be possible to close the terminal window.
The bot once started will create a series of files:

* A file is used to indicate the pid of the process to be able to stop it.
* Then the bot will create a log file that will contain useful information on the actions performed by the bot. (Note that at midnight a new log file is created and by default the bot keeps a maximum of 3 log file backups. However this value can be changed in the bot script.)
* Finally the bot will create a file with the extension ".pickle" to keep track of the users who use the bot. **Removing or modifying this file can affect the correct functioning of the bot**

### User Side
The user who will use the bot will simply have to go to telegram search for the bot and start it. Once started the bot will ask for a security token to be entered to access the data (this token can be set in the script). Once the correct token has been entered, the user will receive the links of the posts published on the chosen subreddit.
The full list of commands is:

* **/help** For more information on how the bot works.
* **/start** To start the bot.
* **/stop** To stop the bot and stop receiving the content (Note that the user will have to re-authenticate to be able to use the content again).
* **/token <token>** To send the authentication token.

There is a secret command in the bot: **/ison**. The latter returns a "Yes" message if the bot is active.
Note that this command is not shown to the user but is intended to be used simply for debugging purposes.

## Built With

* [Visual Studio Code](https://code.visualstudio.com/) - The  code editor.
* [Python](https://www.python.org/) - The language used.

## Versioning
I use [BitBucket](https://bitbucket.org/) for versioning. 

## Author
 **Marco Picariello** 