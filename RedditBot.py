from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from daemonize import Daemonize
import logging
import praw
import threading
import time
import copy
import re
import pickle 
import os

#Bot variables
bot = None
chatIdArray = []
lastPosts=[]
startTime= 0.0
reddit = None
botAPIToken="[INSERT BOT API TOKEN HERE]"

#Bot settings variables
subredditName = "[INSERT THE SUBREDDIT NAME HERE]"
tokenString="[INSERT THE SECRET TOKEN HERE]"
nBackUps = 3
loopTime = 5.0 #in seconds
nWelcomePosts = 10
logFileName="logfile"
idFileName="IdsList"

#Reddit autentication variables (you can get this data from reddit api)
userAgent = "[INSERT USER AGENT HERE]"
clientId = "[INSERT CLIENT ID HERE]"
clientSecret= "[INSERT CLIENT SECRET HERE]"
redditUsername="[INSERT REDDIT USERNAME HERE]"
redditPasswd="[INSERT REDDIT PASSWD HERE]"

def onError(update,context):
	'''
	This method is called whenever an error occurs at the time of a command execution and logs the error.
	'''
	logger.info("==================")
	logger.info(getTime()+"An error occurred while executing a command!")
	logger.info("Type:"+str(context.error))
	logger.info("==================")

def getTime():
	'''
	Get the current time and return it in the format HH:MM:SS
	'''	
	t = time.localtime()
	current_time = time.strftime("%H:%M:%S", t)
	return "["+str(current_time)+"] "

def GetContent():
	'''
	This is the key feature of the bot.
	Check for new posts on the subreddit and send them to all registered users if there are any.
	This operation is repeated every loopTime.
	'''	
	global bot
	global lastPosts

	postLimit=5
	postArray = []
	thereareNewPosts = False
	breaker = False
	send=True
	
	try:
		while(True):
			while(not thereareNewPosts):
				postArray= []
				posts = reddit.subreddit(subredditName).new(limit = postLimit)
				postsBack = copy.deepcopy(posts)  
				for p in posts:
					if(p.id in lastPosts):
						thereareNewPosts = True
						break
					else:
						postArray.append(p) 

				postLimit+=10
				if(postLimit>=150):
					send=False
					break;

			thereareNewPosts = False
			postLimit=5
			if(send):
				if (len(postArray) > 0):
					for i in range(5):
						lastPosts[i] = next(postsBack).id
					for p in postArray:
						logger.info(getTime()+"New content found: "+p.title+" =====> "+p.url)
						for chatId in chatIdArray:
							bot.send_message(chat_id=str(chatId), text=str(p.url)+"\n\n\n"+str(p.title))
			else:
				send=True
				for i in range(5):
					lastPosts[i] = next(postsBack).id


			time.sleep(loopTime - ((time.time()-startTime)%loopTime))
	except Exception as e:
		logger.info("==================")
		logger.info(getTime()+"An error occurred!")
		logger.info(str(e))
		logger.info("==================")	

def Start(update, context):
	'''
	Send a message when the command /start is issued.
	'''
	global chatIdArray	
	chatIdSt = str(update.effective_chat.id)

	if(chatIdSt in chatIdArray):
		update.message.reply_text("Bot is already started.")
	else:
		update.message.reply_text(("Hi, this is the RedditBot!\n\nThis bot is private and "+
								"can be used by invitation, by entering the secret token.\n"+
								"Use the command \"/token <token>\" to use this bot.\n\nIf you have any problem use the command /help for more informations."))

def Help(update, context):
	'''
	Send a message when the command /help is issued to help the user.
	'''	
	update.message.reply_text(("Don't panic!\n\nThanks for your request. Here's how to use the bot:\n\n"+
								"/start -> Activate the bot.\n\n"+
								"/stop -> Stop sending content if you are a registered user, otherwise greet you kindly.\n\n"+
								"/help -> Show this message.\n\n"+
								"/token -> Allows you to authenticate and take advantage of the contents of the bot. "+
								"To use this command, send:\n /token <token> replacing \"<token>\" with the secret access key."))

def IsOn(update, context):
	'''
	Send a message when the command /isOn is issued to check if the bot is on.
	'''
	update.message.reply_text('Yes')

def Token(update, context):
	'''
	Autenticate a new user for getting content from the bot.
	Send the last nWelcomePost posts to the new user.
	'''	
	global tokenString
	global chatIdArray

	messageToken = re.findall('\w+$', update.message.text)[0]
	chatId = str(update.effective_chat.id)

	if(chatId in chatIdArray):
		update.message.reply_text('You are already a member of the RedditBot.')
	else:
		if(tokenString == messageToken):
			logger.info(getTime()+"New User join! -> "+str(chatId))
			chatIdArray.append(chatId)

			pickle.dump(chatIdArray, open(cwd+'/'+idFileName+'.pickle',"wb"))
			
			introposts = reddit.subreddit(subredditName).new(limit = nWelcomePosts)
			logger.info(getTime()+"Sending welcome posts...")
			for p in introposts:
				bot.send_message(chat_id=str(chatId), text=str(p.url)+"\n\n\n"+str(p.title))
			update.message.reply_text("Welcome to the RedditBot!\n\nFrom this moment you will receive the contents of the bot.\n\nUse the /help command to find out what the bot can do!")
			update.message.reply_text("I sent you some of the posts you missed while you were gone. Enjoy!")	
		else:
			update.message.reply_text('Wrong token.\nTry anagin')

def Stop(update, context):
	'''
	Remove a user from the autenticated users ad send a message
	'''	
	global chatIdArray
	chatIdRm = str(update.effective_chat.id)

	if(chatIdRm in chatIdArray):
		logger.info(getTime()+"User left -> "+str(chatIdRm))
		chatIdArray.remove(chatIdRm)
		
		pickle.dump(chatIdArray, open(cwd+'/'+idFileName+'.pickle',"wb"))
		
		update.message.reply_text("From this moment you will no longer receive the contents of the bot.\n\n To use the RedditBot again, you will need to re-authenticate by sending the token.")
	else:
		update.message.reply_text("See you later!")

def main():
	'''
	The main function
	'''	
	global bot
	global reddit
	global lastPosts
	global loopTime
	global startTime
	global subredditName
	global chatIdArray
	global cwd

	logger.info(getTime()+"Bot Started with pid: "+str(os.getpid()))

	#Initialize a new object for get content from reddit
	reddit = praw.Reddit(user_agent=userAgent,
	             client_id=clientId, 
	             client_secret=clientSecret,
	             username=redditUsername, 
	             password=redditPasswd)


	#Get the last 5 posts
	recentPosts = reddit.subreddit(subredditName).new(limit = 5)
	for p in recentPosts:
		lastPosts.append(p.id)

	#Initialize the bot
	updater = Updater(botAPIToken, use_context=True)
	bot = updater.bot

	
	#Manage the known Ids file
	if os.path.isfile(cwd+'/'+idFileName+'.pickle'):
		if(os.stat(cwd+'/'+idFileName+'.pickle').st_size == 0):      
			logger.info(getTime()+"The file is empty")
		else:    
			chatIdArray = pickle.load(open(cwd+'/'+idFileName+'.pickle',"r+b"))
			logger.info(getTime()+"Known Ids loaded")
	else:
		logger.info(getTime()+"The Ids file does not exist, a new one will be created")
	
	#Start retriving contents
	timer = threading.Timer(0.0, GetContent)
	startTime = time.time()
	timer.daemon = True
	timer.start() 

	
	#Manage the command of the bot
	dp = updater.dispatcher
	dp.add_handler(CommandHandler("start", Start))
	dp.add_handler(CommandHandler("stop", Stop))
	dp.add_handler(MessageHandler(Filters.regex('^(/token\s\w+)$'), Token))
	dp.add_handler(CommandHandler("help", Help))
	dp.add_handler(CommandHandler("ison", IsOn))
	dp.add_error_handler(onError)
	updater.start_polling()
	updater.idle()

logging.basicConfig(
    filename= logFileName+'.txt',
    level=logging.INFO
)
logger = logging.getLogger(__name__)

#A handler is given to the logger and the latter will take care of managing the log files.
#Log files are at most nBackUps and a new file is created at midnight every day.
handler = logging.handlers.TimedRotatingFileHandler('./'+logFileName+'.txt', when="midnight", backupCount=nBackUps)
logger.addHandler(handler)
keep_fds = [handler.stream.fileno()]

#Execute the main
if __name__ == '__main__':
	cwd = str(os.getcwd())
	daemon = Daemonize(app="DaemonBot", pid=str(os.getpid()), action=main, keep_fds=keep_fds, logger= logger)
	daemon.start()